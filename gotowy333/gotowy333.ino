#include <Wire.h> //This library allows you to communicate with I2C / TWI devices.
#include <LiquidCrystal_I2C.h> //Biblioteka LiquidCrystal_I2C.h zawiera szereg funkcji przydatnych gdy w swoim projekcie wykorzystujemy popularne wyświetlacze tekstowe LCD
LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display //funkcja w LQ
int Pin1 = 10;//IN1 podlaczone do 10 //zmienne pomocnicze
int Pin2 = 11;//IN2 podlaczone do 11 //przycisk podłączony do pinu 
int Pin3 = 12;//IN3 podlaczone do 12
int Pin4 = 13;//IN4 podlaczone do 13
int przyciskTyl  = 4; // przycisk zgodnie z ruchem wskazówek zegara
int switchStop = 3; //przycisk stop
int przyciskPrzod = 2; //przycisk przeciwnie do ruchu wskazówek zegara
int limitTyl = 28; // czujnik krańcowy
int limitPrzod = 26; // czujnik krańcowy
int pole1[] = {0, 0, 0, 0, 0, 1, 1, 1, 0}; //biegun1, 8 wartości krokowych
int pole2[] = {0, 0, 0, 1, 1, 1, 0, 0, 0}; //biegun2, 8 wartości krokowych
int pole3[] = {0, 1, 1, 1, 0, 0, 0, 0, 0}; //biegun3, 8 wartości krokowych
int pole4[] = {1, 1, 0, 0, 0, 0, 0, 1, 0}; //biegun4, 8 wartości krokowych
enum stan { //typ - wartosc
  przod,
  tyl,
  zatrzymaj
};

int poleStep = 0; // zmienna 
stan  dirStatus = zatrzymaj; // przechowuje status ropoczęcia
void setup() //funcja 
{
  pinMode(Pin1, OUTPUT);//wyjście 
  pinMode(Pin2, OUTPUT);
  pinMode(Pin3, OUTPUT);
  pinMode(Pin4, OUTPUT);
  pinMode(przyciskPrzod, INPUT_PULLUP); //WEJŚCIE_Z_PODCIĄGNIĘCIEM] - ustawia pin jako wejście z domyślnym stanem HIGH. Stan zmienia podłączenie wejścia do GND. 
  pinMode(switchStop, INPUT_PULLUP);
  pinMode(przyciskTyl, INPUT_PULLUP);
  pinMode(limitTyl, INPUT_PULLUP);
  pinMode(limitPrzod, INPUT_PULLUP);
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  lcd.setCursor(5,0);
  lcd.print("Witaj");
  lcd.setCursor(2,1);
  lcd.print("urzytkowniku!");
}
void loop() //funcja
{
  if (digitalRead(limitTyl) == LOW && digitalRead(przyciskPrzod) == LOW) //digitalRead [ang. odczytaj z pinu cyfrowego] - odczytuje wartość wejścia pinu cyfrowego.
  {
    dirStatus = przod;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("dol");
  } else if (digitalRead(limitPrzod) == LOW && digitalRead(przyciskTyl) == LOW)
  {
    dirStatus = tyl;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("gora");
  } else if ((digitalRead(przyciskTyl) == LOW) && (digitalRead(limitTyl) == HIGH))
  {
    dirStatus = tyl;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("gora");
  } else if ((dirStatus == przod && digitalRead(limitPrzod) == LOW))
  {
    dirStatus = zatrzymaj;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("stop");
    lcd.setCursor(1,1);
    lcd.print("granica dol");
  } else if ((digitalRead(przyciskPrzod) == LOW)&& (digitalRead(limitPrzod) == HIGH))
  {
    dirStatus  = przod;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("dol");
  } else if ((dirStatus == tyl && digitalRead(limitTyl) == LOW))
  {
    dirStatus = zatrzymaj;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("stop");
    lcd.setCursor(1,1);
    lcd.print("granica gora");
  } else if (digitalRead(switchStop) == LOW)
  {
    dirStatus = zatrzymaj;
    lcd.clear();
    lcd.setCursor(5,0);
    lcd.print("stop");
  }
  if (dirStatus == przod) { //warunek
    poleStep++;
    driveStepper(poleStep);
  } else if (dirStatus == tyl) {
    poleStep--;
    driveStepper(poleStep);
  } else {
    driveStepper(8);
  }
  if (poleStep > 7) { // reset
    poleStep = 0;
  }
  if (poleStep < 0) { //warunek resetujacy
    poleStep = 7;
  }
  delay(1);
}// loop


void driveStepper(int c)//driveStepper //funkcja 
{
  
  digitalWrite(Pin1, pole1[c]);
  digitalWrite(Pin2, pole2[c]);
  digitalWrite(Pin3, pole3[c]);
  digitalWrite(Pin4, pole4[c]);
}
